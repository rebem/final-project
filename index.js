async function getNPIData() {
  const search = document.querySelector("input");

  const url = `https://clinicaltables.nlm.nih.gov/api/npi_org/v3/search?terms=${search.value}&ef=other_ids`;
  const response = await fetch(url);

  const data = await response.json();


  return data;
}

document.querySelector("button").addEventListener("click", async () => {
  const data = await getNPIData();
  console.log(data);

  const providerData = data[3];
  const table = document.getElementById("table-body");
  for (let i = 0; i < providerData.length; i++) {
    const npiID = providerData[i][1];
    const name = providerData[i][0];
    const type = providerData[i][2];
    const address = providerData[i][3];

    const idHeader = document.getElementById("NPI-ID");
    const nameHeader = document.getElementById("Name");
    const typeHeader = document.getElementById("Type");
    const addressHeader = document.getElementById("Address");

    const row = document.createElement("tr");

    const IDRowItem = document.createElement("td");
    const IDRowText = document.createTextNode(npiID);
    const addressRowText = document.createTextNode(address);
    const addressRowItem = document.createElement("td");
    const nameRowItem = document.createElement("td");
    const nameRowText = document.createTextNode(name);
    const typeRowItem = document.createElement("td");
    const typeRowText = document.createTextNode(type);

    IDRowItem.appendChild(IDRowText);
    row.appendChild(IDRowItem);
    nameRowItem.appendChild(nameRowText);
    row.appendChild(nameRowItem);
    typeRowItem.appendChild(typeRowText);
    row.appendChild(typeRowItem);
    addressRowItem.appendChild(addressRowText);
    row.appendChild(addressRowItem);
    table.appendChild(row);
  }
});
